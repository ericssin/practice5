# Heading1
Normal paragraph text

## Heading 2

**formatted text** and other _formats_

+ list
+ items

1. numbered
2. lists

|header 1 |header 2|
|---------|--------|
|cell1    | cell 2 |
|row 2    | row  2 |


[link text](www.google.com)

[img/image.jpg]

on this paragraph we have ```inline code``` as backticks

```
/// A Code Block goes here
    let firstName='namex'    
```